#!/bin/bash
cd /usr/src/linux &&
VER=`make kernelversion` &&
cd /boot &&
dracut '' $VER --force --fstab -M $@
