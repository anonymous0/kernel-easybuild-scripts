#!/bin/bash
set +x
KERNEL_SRC="/usr/src/linux"
CUSTOM_CONFIG="easybuild.config"

cd $KERNEL_SRC &&
rm -f .config &&
make defconfig &&
kernel-configurator &&
mv .config $CUSTOM_CONFIG &&
KCONFIG_ALLCONFIG=$CUSTOM_CONFIG make alldefconfig &&
rm $CUSTOM_CONFIG
